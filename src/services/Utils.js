export default class Utils {
  static isDevEnv() {
    return process.env.NODE_ENV !== "production"
  }

  static replaceSlash(value) {
    return value.replace(/\//g, '')
  }

  static validateEmail(email) {
    const regex = /^(([^<>()[\]\\.,:\s@"]+(\.[^<>()[\]\\.,:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return regex.test(String(email).toLowerCase().trim())
  }

  static validateSiret(siret) {
    if (siret.length === 14) {
      return true
    }

    return false
  }

  static validatePhoneNumber(phone) {
    const regex = /^((\+)33|0)[1-9](\d{2}){4}$/g
    return regex.test(phone)
  }

  static isJSONString(string) {
    try {
      JSON.parse(string)
    } catch (e) {
      return false
    }
    
    return true
  }
}
