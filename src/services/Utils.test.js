import Utils from "./Utils"

test("Validate email", () => {
  const goodEmail = "name@domain.com"
  const wrongEmail = "name@domain"
  const otherWrongEmail = "name.domain.com"
  expect(Utils.validateEmail(goodEmail)).toBe(true)
  expect(Utils.validateEmail(wrongEmail)).toBe(false)
  expect(Utils.validateEmail(otherWrongEmail)).toBe(false)
})

test("Remove slash", () => {
  const sentence = "Hell/o,/ this is a t/est./"
  expect(Utils.replaceSlash(sentence)).toBe("Hello, this is a test.")
})