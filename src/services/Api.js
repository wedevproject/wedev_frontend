import axios from "axios"
const url = "http://api.we-are-dev.com/api/v1"

export default class Api {
  static get(endpoint, params) {
    return axios.get(url + endpoint, params)
  }

  static post(endpoint, params) {
    return axios.post(url + endpoint, params)
  }

  static put(endpoint, params) {
    return axios.put(url + endpoint, params)
  }

  static delete(endpoint, params) {
    return axios.delete(url + endpoint, params)
  }
}