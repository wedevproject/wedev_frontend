import "./project.scss"
import { Link } from "react-router-dom"
import AppLayout from "../../../components/appLayout/AppLayout"
import Button from "../../../components/button/Button"
import React from "react"
import Textfield from "../../../components/textfield/Textfield"

const Project = (project) => {
  const handleSubmit = (e) => {
    e.preventDefault()
  }

  const selectOptionsStatus = [
    {value: "First value", label: "First label"},
    {value: "Second value", label: "Second label"},
  ]

  const selectOptionsClient = [
    {value: "First value", label: "First label"},
    {value: "Second value", label: "Second label"},
  ]

  const selectOptionsStack = [
    {value: "First value", label: "First label"},
    {value: "Second value", label: "Second label"},
  ]

  return (
    <AppLayout
      title="Détail du projet : Nom du projet"
      actionLabel="Tâches du projet"
      actionUrl="/tasks"
    >
      <div className="link-space"><Link to="/projects">Retour</Link></div>
      <form>
        <Textfield label="Titre du projet" placeholder="Titre..." />
        <Textfield label="Montant du devis" placeholder="Devis en euos..." />
        <Textfield label="Délai de réalisation" type="number" placeholder="Nombres de jours..." />
        <Textfield type="select" options={selectOptionsStatus} label="Statut" placeholder="Ex: En cours..." />
        <Textfield label="Date de début" type="date" placeholder="Début..." />
        <Textfield label="Date de fin" type="date" placeholder="Fin..." />
        <Textfield label="Coût horaire par jour" type="number" placeholder="Ex: 400" />
        <Textfield type="select" options={selectOptionsClient} label="Client" placeholder="Ex: Groupe PSA..." />
        <Textfield type="select" options={selectOptionsStack} label="Stack utilisée" placeholder="Ex: Javascript, GoLang..." />
        <div className="line">
          <Button color="emeraud" click={handleSubmit}>Enregistrer</Button>
          <Button color="red" click={handleSubmit} className="space">Supprimer</Button>
        </div>
      </form>
    </AppLayout>
  )
}

export default Project