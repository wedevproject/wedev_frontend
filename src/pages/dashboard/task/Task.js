import "./task.scss"
import { Link } from "react-router-dom"
import AppLayout from "../../../components/appLayout/AppLayout"
import Button from "../../../components/button/Button"
import React from "react"
import Textfield from "../../../components/textfield/Textfield"

const Task = (task) => {
  const handleSubmit = (e) => {
    e.preventDefault()
  }

  const selectOptionsStatus = [
    {value: "First value", label: "First label"},
    {value: "Second value", label: "Second label"},
  ]

  return (
    <AppLayout title="Détail de la tâche : Nom de la tâche">
      <div className="link-space"><Link to="/tasks">Retour</Link></div>
      <form>
        <Textfield label="Titre de la tâche" placeholder="Titre..." />
        <Textfield label="Description de la tâche" placeholder="Description..." />
        <Textfield type="select" options={selectOptionsStatus} label="Statut" placeholder="Ex: En cours..." />
        <Textfield label="Temps de réalisation (en heures)" type="number" placeholder="Temps de réalisation..." />
        <div className="line">
        <Button color="emeraud" click={handleSubmit}>Enregistrer</Button>
        <Button color="red" click={handleSubmit} className="space">Supprimer</Button>
        </div>
      </form>
    </AppLayout>
  )
}

export default Task