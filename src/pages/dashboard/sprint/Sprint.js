import "./sprint.scss"
import { Link } from "react-router-dom"
import AppLayout from "../../../components/appLayout/AppLayout"
import Button from "../../../components/button/Button"
import React from "react"
import Textfield from "../../../components/textfield/Textfield"

const Sprint = (sprint) => {
  const handleSubmit = (e) => {
    e.preventDefault()
  }

  const selectOptionsStatus = [
    {value: "First value", label: "First label"},
    {value: "Second value", label: "Second label"},
  ]

  return (
    <AppLayout
      title="Détail du sprint : Nom du sprint"
      actionLabel="Tâches du projet"
      actionUrl="/tasks"
    >
      <div className="link-space"><Link to="/sprints">Retour</Link></div>
      <form>
        <Textfield label="Titre du Sprint" placeholder="Titre du sprint..." />
        <Textfield label="Date de Début" type="date" placeholder="Date de Début..." />
        <Textfield label="Date de Fin" type="date" placeholder="Date de Fin..." />
        <Textfield type="select" options={selectOptionsStatus} label="Statut" placeholder="Ex: En cours..." />
        <div className="line">
          <Button color="emeraud" click={handleSubmit}>Enregistrer</Button>
          <Button color="red" click={handleSubmit} className="space">Supprimer</Button>
        </div>
      </form>
    </AppLayout>
  )
}

export default Sprint