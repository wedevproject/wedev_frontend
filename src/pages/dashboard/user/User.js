import "./user.scss"
import { useMappedState } from "redux-react-hook"
import { withCookies } from "react-cookie"
import Api from "../../../services/Api"
import AppLayout from "../../../components/appLayout/AppLayout"
import Button from "../../../components/button/Button"
import React, { useState, useEffect } from "react"
import Textfield from "../../../components/textfield/Textfield"

const User = ({ cookies, history }) => {
  const mapState = state => ({ id: state.id })
  const { id } = useMappedState(mapState)
  const [getLastname, setLastname] = useState('')
  const [getFirstname, setFirstname] = useState('')
  const [getEmail, setEmail] = useState('')
  const [getPhone, setPhone] = useState('')
  const [getProfileId, setProfileId] = useState()
  const [getProfileName, setProfileName] = useState()
  const [getTempOptions, setTempOptions] = useState([])
  let options = []
  let defaultOption = {}

  const handleChangeInput = input => (value) => {
    switch (input) {
      case 'lastname':
        setLastname(value)
        break
      case 'firstname':
        setFirstname(value)
        break
      case 'email':
        setEmail(value)
        break
      case 'phone':
        setPhone(value)
        break
      case 'profile':
        const isProfile = (profile) => {
          return profile.value === value
        }
        
        const label = options.find(isProfile).label
        setProfileId(value)
        setProfileName(label)
        break
      default:
        return false
    }
  }

  const handleSubmit = async (e) => {
    e.preventDefault()

    return await Api.post(`/user/${id}`, {
      firstName: (getFirstname ? getFirstname : undefined),
      lastName: (getLastname ? getLastname : undefined),
      email: (getEmail ? getEmail : undefined),
      phone: (getPhone ? getPhone : undefined),
      profileId: (getProfileId ? getProfileId : undefined),
    })
  }

  const handleLogout = () => {
    cookies.remove('token')
    history.push('/')
  }

  useEffect(() => {
    Api.get(`/user/${id}`)
      .then((res) => {
        const data = res.data
        setLastname(data.lastName)
        setFirstname(data.firstName)
        setEmail(data.email)
        setPhone(data.phone)
        setProfileId(data.profile.id)
        setProfileName(data.profile.name)
      })
      .catch(() => {
        console.error('Une erreur est survenue, merci de réessayer plus tard')
      })

    Api.get('/profile')
      .then((res) => {
        const profiles = res.data
        const tempProfiles = []
        profiles.forEach((profile) => tempProfiles.push({value: profile.id, label: profile.name}))
        setTempOptions(tempProfiles)
      })
      .catch(() => {
        console.error('Une erreur est survenue, merci de réessayer plus tard')
      })
  }, [id])

  if (getProfileId && getProfileName && getTempOptions) {
    defaultOption = {value: getProfileId, label: getProfileName}
    options = getTempOptions
  }

  return (
    <AppLayout title="Mon compte">
      <form>
        <Textfield label="Votre nom" placeholder="Nom..." value={getLastname} change={handleChangeInput('lastname')} />
        <Textfield label="Votre prénom" placeholder="Prénom..." value={getFirstname} change={handleChangeInput('firstname')} />
        <Textfield label="Votre adresse mail" type="email" placeholder="Adresse mail..." value={getEmail} change={handleChangeInput('email')} />
        <Textfield label="Votre numéro de téléphone" type="tel" placeholder="0633..." value={getPhone} change={handleChangeInput('phone')} />
        <Textfield type="select" options={options} value={defaultOption} label="Votre profil" placeholder="Développeur back-end..." change={handleChangeInput('profile')} />
        <div className="line">
          <Button color="emeraud" click={handleSubmit}>Enregistrer</Button>
          <Button color="blue" className="space" click={handleLogout}>Déconnexion</Button>
        </div>
      </form>
    </AppLayout>
  )
}

export default withCookies(User)