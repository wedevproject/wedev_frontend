import "./client.scss"
import { Link } from "react-router-dom"
import AppLayout from "../../../components/appLayout/AppLayout"
import Button from "../../../components/button/Button"
import React from "react"
import Textfield from "../../../components/textfield/Textfield"

const Client = (client) => {
  const handleSubmit = (e) => {
    e.preventDefault()
  }

  const selectOptions = [
    {value: "First value", label: "First label"},
    {value: "Second value", label: "Second label"},
  ]

  return (
    <AppLayout title="Détail du client : Nom du client">
      <div className="link-space"><Link to="/clients">Retour</Link></div>
      <form>
        <Textfield type="select" options={selectOptions} label="Dénomination sociale" placeholder="Dénomination..." />
        <Textfield label="Adresse postale" placeholder="Adresse..." />
        <Textfield label="Nom du contact" placeholder="Nom..." />
        <Textfield label="Prénom du contact" placeholder="Prénom..." />
        <Textfield label="Numéro de téléphone" type="tel" placeholder="0180..." />
        <Textfield label="Adresse mail" type="email" placeholder="Adresse mail..." />
        <div className="line">
          <Button color="emeraud" click={handleSubmit}>Enregistrer</Button>
          <Button color="red" click={handleSubmit} className="space">Supprimer</Button>
        </div>
      </form>
    </AppLayout>
  )
}

export default Client