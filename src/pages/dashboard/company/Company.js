import { useMappedState } from "redux-react-hook"
import Api from "../../../services/Api"
import AppLayout from "../../../components/appLayout/AppLayout"
import Button from "../../../components/button/Button"
import React, { useState, useEffect } from "react"
import Textfield from "../../../components/textfield/Textfield"

const Company = () => {
  const mapState = state => ({ id: state.id })
  const { id } = useMappedState(mapState)
  const [getCompany, setCompany] = useState('')
  const [getSiret, setSiret] = useState('')
  const [getStatusId, setStatusId] = useState()
  const [getStatusName, setStatusName] = useState()
  const [getTempOptions, setTempOptions] = useState([])
  let options = []
  let defaultOption = {}

  const handleChangeInput = input => (value) => {
    switch (input) {
      case 'company':
        setCompany(value)
        break
      case 'siret':
        setSiret(value)
        break
      case 'status':
        const isStatus = (status) => {
          return status.value === value
        }
        
        const label = options.find(isStatus).label
        setStatusId(value)
        setStatusName(label)
        break
      default:
        return false
    }
  }

  const handleSubmit = async (e) => {
    e.preventDefault()

    return await Api.post(`/user/${id}`, {
      company: (getCompany ? getCompany : undefined),
      siret: (getSiret ? getSiret : undefined),
      stateSocietyId: (getStatusId ? getStatusId : undefined),
    })
  }

  useEffect(() => {
    Api.get(`/user/${id}`)
      .then((res) => {
        const data = res.data
        setCompany(data.company)
        setSiret(data.siret)
        setStatusId(data.stateSociety.id)
        setStatusName(data.stateSociety.name)
      })
      .catch(() => {
        console.error('Une erreur est survenue, merci de réessayer plus tard')
      })

    Api.get('/state_society')
    .then((res) => {
      const status = res.data
      const tempStatus = []
      status.forEach((state) => tempStatus.push({value: state.id, label: state.name}))
      setTempOptions(tempStatus)
    })
    .catch(() => {
      console.error('Une erreur est survenue, merci de réessayer plus tard')
    })
  }, [id])

  if (getStatusId && getStatusName && getTempOptions) {
    defaultOption = {value: getStatusId, label: getStatusName}
    options = getTempOptions
  }

  return (
    <AppLayout title="Informations de ma société">
      <form>
        <Textfield label="Nom de la société" placeholder="Société..." value={getCompany} change={handleChangeInput('company')} />
        <Textfield type="select" options={options} value={defaultOption} label="Statut de société" placeholder="Statut..." change={handleChangeInput('status')} />
        <Textfield label="Numéro de SIRET" placeholder="SIRET..." value={getSiret} change={handleChangeInput('siret')} />
        <div className="line">
          <Button color="emeraud" click={handleSubmit}>Enregistrer</Button>
        </div>
      </form>
    </AppLayout>
  )
}

export default Company