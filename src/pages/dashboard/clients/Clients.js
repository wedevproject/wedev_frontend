import "./clients.scss"
import Api from "../../../services/Api"
import AppLayout from "../../../components/appLayout/AppLayout"
import Button from "../../../components/button/Button"
import clsx from "clsx"
import React, { useState, useEffect } from "react"

const Clients = () => {
  const [getModal, setModal] = useState(false)

  const handleAddClient = (e) => {
    e.preventDefault()
  }

  const handleShowModal = () => {
    setModal(true)
  }

  useEffect(() => {
    Api.get(`/customer`)
      .then((res) => {
        const data = res.data
        console.log(data)
      })
      .catch(() => {
        console.error('Une erreur est survenue, merci de réessayer plus tard')
      })
  }, [])

  return (
    <AppLayout
      title="Gestion des clients"
    >
      <div className="block">

        <div className="line">
          <div className={clsx('modal', {'modal-active': getModal})}>
            <li>Editer le client</li>
            <li>Supprimer le client</li>
          </div>
          <div className="group">Nom du client</div>
          <div className="group"><span onClick={handleShowModal}>...</span></div>
        </div>

        <div className="line">
          <div className={clsx('modal', {'modal-active': getModal})}>
            <li>Editer le client</li>
            <li>Supprimer le client</li>
          </div>
          <div className="group">Nom du client</div>
          <div className="group"><span onClick={handleShowModal}>...</span></div>
        </div>

        <div className="line">
          <div className={clsx('modal', {'modal-active': getModal})}>
            <li>Editer le client</li>
            <li>Supprimer le client</li>
          </div>
          <div className="group">Nom du client</div>
          <div className="group"><span onClick={handleShowModal}>...</span></div>
        </div>
        
        <div className="actions"><Button color="emeraud" click={handleAddClient}>Ajouter un client</Button></div>
      </div>
    </AppLayout>
  )
}

export default Clients