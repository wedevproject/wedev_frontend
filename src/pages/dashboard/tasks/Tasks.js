import "./tasks.scss"
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd"
import AppLayout from "../../../components/appLayout/AppLayout"
import React, { useState } from "react"
import Badge from "../../../components/badge/Badge"

const Task = () => {
  const [items, setItems] = useState([
    { id: '0', content: "Première tâche" },
    { id: '1', content: "Deuxième tâche" },
    { id: '2', content: "Troisième tâche" },
    { id: '3', content: "Quatrième tâche" },
    { id: '4', content: "Cinquième tâche" },
  ])

  //const [modal, setModal] = useState(false)
  const getItemStyle = (isDragging, draggableStyle) => ({
    userSelect: "none",
    background: isDragging && "rgba(32,145,220, 0.07)",
    ...draggableStyle,
  })

  const handleOnDragEnd = e => {
    if (!e.destination) {
      return
    }

    const source = e.source.index
    const destination = e.destination.index
    const result = Array.from(items)
    const [removed] = result.splice(source, 1)
    result.splice(destination, 0, removed)

    setItems(result)
  }

  return (
    <AppLayout title="Liste des tâches pour le projet « Ynov campus »">
      <div className="dnd-list">
        <DragDropContext onDragEnd={handleOnDragEnd}>
          <Droppable droppableId="droppable">
            {(provided, snapshot) => (
              <div
                {...provided.droppableProps}
                ref={provided.innerRef}
              >
                {items.map((item, index) => {
                  const id = item.id
                  return (
                    <Draggable key={id} draggableId={id} index={index}>
                      {(provided, snapshot) => (
                        <div
                          id={id}
                          ref={provided.innerRef}
                          className="dnd-element"
                          {...provided.draggableProps}
                          {...provided.dragHandleProps}
                          style={getItemStyle(
                            snapshot.isDragging,
                            provided.draggableProps.style
                          )}
                        >
                          <div>
                            {item.content}
                            <Badge color="blue">Front-end</Badge>
                            <Badge color="lightGrey">2 jours</Badge>
                          </div>
                          <div onClick={() => alert('ok')}><span>...</span></div>
                          <div className="modal" id={id}>test</div>
                        </div>
                      )}
                    </Draggable>
                  )}
                )}
                {provided.placeholder}
              </div>
            )}
          </Droppable>
        </DragDropContext>
      </div>
    </AppLayout>
  )
}

export default Task