import { Link } from "react-router-dom"
import AuthLayout from "../../components/authLayout/AuthLayout"
import Button from "../../components/button/Button"
import mockupImage from "../../assets/images/auth/mockup.svg"
import React, { useState, useEffect } from "react"
import Textfield from "../../components/textfield/Textfield"
import Utils from "../../services/Utils"
import { useDispatch } from "redux-react-hook"
import actions from "../../store/action"

const Password = () => {
  const [getEmail, setEmail] = useState()
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch({ type: actions.SET_ERROR, value: 'Cette page n\'est pas encore utilisable.' })
  }, [dispatch])

  const handleChangeEmail = (value) => {
    setEmail(value)
  }

  const handleSubmit = (e) => {
    e.preventDefault()

    if (Utils.validateEmail(getEmail)) {
      dispatch({ type: actions.SET_SUCCESS, value: 'Vous allez recevoir un mail de réinitialisation.' })
    }
    else {
      dispatch({ type: actions.SET_ERROR, value: 'Adresse mail invalide.' })
    }
  }

  return (
    <AuthLayout
      title="Mot de passe oublié"
      image={mockupImage}
      text={
        <>
          Connectez-vous pour accéder à notre outil de gestion de projet pour développeurs.<br/>
          Simplifiez vos développements, obtenez des statistiques poussés et bien d'autres choses...
        </>
      }
    >
      <Textfield label="Votre adresse mail" change={handleChangeEmail} placeholder="Adresse mail..." />
      <div className="center"><Button color="emeraud" click={handleSubmit}>Envoyer</Button></div>
      <div className="center"><Link to="/login" className="link-space">Je connais mon mot de passe</Link></div>
    </AuthLayout>
  )
}

export default Password