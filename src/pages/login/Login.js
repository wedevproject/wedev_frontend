import { Link, withRouter } from "react-router-dom"
import { useDispatch } from "redux-react-hook"
import { withCookies } from "react-cookie"
import actions from "../../store/action"
import Api from "../../services/Api"
import AuthLayout from "../../components/authLayout/AuthLayout"
import Button from "../../components/button/Button"
import mockupImage from "../../assets/images/auth/mockup.svg"
import React, { useState } from "react"
import Textfield from "../../components/textfield/Textfield"
import Utils from "../../services/Utils"

const Login = ({ cookies, history }) => {
  const [getEmail, setEmail] = useState()
  const [getPassword, setPassword] = useState()

  const dispatch = useDispatch()
  
  const handleChangeInput = input => (value) => {
    switch (input) {
      case 'email':
        setEmail(value)
        break
      case 'password':
        setPassword(value)
        break
      default:
        return false
    }
  }

  const handleError = (error) => {
    dispatch({ type: actions.SET_ERROR, value: null })
    dispatch({ type: actions.SET_ERROR, value: error })
  }

  const handleSubmit = (e) => {
    e.preventDefault()

    if (getEmail && getPassword) {
      const email = Utils.replaceSlash(getEmail.trim().toLowerCase())
      const password = getPassword.trim()

      if (Utils.validateEmail(email)) {
        Api.post('/user/login', {
          email: email,
          password: password,
        })
        .then((res) => {
          cookies.set('token', res.data, { path: '/' })
          history.push('/company')
        })
        .catch(() => {
          handleError('Mauvaise adresse mail ou mauvais mot de passe.')
        })
      }
      else {
        handleError('Adresse mail invalide.')
      }
    }
    else {
      handleError('Veuillez saisir tous les champs.')
    }
  }

  return (
    <AuthLayout
      title="Connexion"
      image={mockupImage}
      text={
        <>
          Connectez-vous pour accéder à notre outil de gestion de projet pour développeurs.<br/>
          Simplifiez vos développements, obtenez des statistiques poussés et bien d'autres choses...
        </>
      }
      backup={<p>Mot de passe oublié ?<br /><Link to="/password">Demandez-en un nouveau</Link></p>}
    >
      <Textfield label="Votre adresse mail" change={handleChangeInput('email')} placeholder="Adresse mail..." />
      <Textfield label="Votre mot de passe" type="password" change={handleChangeInput('password')} placeholder="Mot de passe..." />
      <div className="center"><Button color="emeraud" click={handleSubmit}>Se connecter</Button></div>
      <div className="center"><Link to="/register" className="link-space">Je n'ai pas de compte</Link></div>
    </AuthLayout>
  )
}

export default withCookies(withRouter(Login))