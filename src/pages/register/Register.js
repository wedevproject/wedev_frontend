import { Link } from "react-router-dom"
import { useDispatch } from "redux-react-hook"
import actions from "../../store/action"
import Api from "../../services/Api"
import AuthLayout from "../../components/authLayout/AuthLayout"
import Button from "../../components/button/Button"
import mockupImage from "../../assets/images/auth/mockup.svg"
import React, { useState, useEffect } from "react"
import Textfield from "../../components/textfield/Textfield"
import Utils from "../../services/Utils"

const Register = () => {
  const [getStep, setStep] = useState(1)
  const [getLastname, setLastname] = useState()
  const [getFirstname, setFirstname] = useState()
  const [getProfile, setProfile] = useState()
  const [getPhone, setPhone] = useState()
  const [getEmail, setEmail] = useState()
  const [getPassword, setPassword] = useState()
  const [getCompany, setCompany] = useState()
  const [getSiret, setSiret] = useState()
  const [getStatus, setStatus] = useState()
  const selectOptionsProfile = []
  const selectOptionsStatus = []
  const dispatch = useDispatch()

  const handleChangeInput = input => (value) => {
    switch (input) {
      case 'lastname':
        setLastname(value)
        break
      case 'firstname':
        setFirstname(value)
        break
      case 'profile':
        setProfile(value)
        break
      case 'phone':
        setPhone(value)
        break
      case 'email':
        setEmail(value)
        break
      case 'password':
        setPassword(value)
        break
      case 'company':
        setCompany(value)
        break
      case 'siret':
        setSiret(value)
        break
      case 'status':
        setStatus(value)
        break
      default:
        return false
    }
  }

  const handlePreviousStep = () => {
    const previousStep = getStep - 1
    setStep(previousStep)
  }

  const handleNextStep = (e) => {
    e.preventDefault()
    const nextStep = getStep + 1
    setStep(nextStep)
  }

  const handleAlert = (type, message) => {
    if (type === "error") {
      dispatch({ type: actions.SET_ERROR, value: null })
      dispatch({ type: actions.SET_ERROR, value: message })
    }
    else {
      dispatch({ type: actions.SET_SUCCESS, value: null })
      dispatch({ type: actions.SET_SUCCESS, value: message })
    }
  }

  const handleSubmit = (e) => {
    e.preventDefault()

    if (getLastname && getFirstname && getProfile && getPhone && getEmail && getPassword && getCompany && getSiret && getStatus) {
      const lastname = getLastname.trim()
      const firstname = getFirstname.trim()
      const profile = getProfile
      const phone = Utils.replaceSlash(getPhone.trim().toLowerCase())
      const email = Utils.replaceSlash(getEmail.trim().toLowerCase())
      const password = getPassword.trim()
      const company = getCompany.trim()
      const siret = Utils.replaceSlash(getSiret.trim().toLowerCase())
      const status = getStatus

      if (Utils.validateSiret(siret)) {
        if (Utils.validatePhoneNumber(phone)) {
          if (Utils.validateEmail(email)) {
            Api.put('/user', {
              first_name: firstname,
              last_name: lastname,
              society: company,
              email: email,
              phone: phone,
              password: password,
              password_confirmed: password,
              siret: siret,
              rank_id: 1,
              profile_id: profile,
              state_society_id: status,
            })
            .then(() => {
              handleAlert('success', 'Inscription réalisée avec succès. Vous pouvez vous connecter.')
            })
            .catch((err) => {
              console.log(err.response)
              handleAlert('error', 'Une erreur est surevenue.')
            })
          }
          else {
            handleAlert('error', 'Adresse mail invalide.')
          }
        }
        else {
          handleAlert('error', 'Numéro de téléphone invalide.')
        }
      }
      else {
        handleAlert('error', 'Numéro de SIRET invalide.')
      }
    }
    else {
      handleAlert('error', 'Veuillez saisir tous les champs.')
    }
  }

  useEffect(() => {
    Api.get('/profile')
      .then((res) => {
        const profiles = res.data
        profiles.forEach((profile) => selectOptionsProfile.push({value: profile.id, label: profile.name}))
      })
      .catch(() => {
        handleAlert('error', 'Une erreur est surevenue.')
      })

    Api.get('/state_society')
      .then((res) => {
        const profiles = res.data
        profiles.forEach((state) => selectOptionsStatus.push({value: state.id, label: state.name}))
      })
      .catch(() => {
        handleAlert('error', 'Une erreur est surevenue.')
      })
  })

  return (
    <AuthLayout
      title="Inscription"
      image={mockupImage}
      text={
        <>
          Inscrivez-vous dès maintenant pour accéder à notre outil de gestion de projet pour développeurs.<br/>
          Simplifiez vos développements, obtenez des statistiques poussés et bien d'autres choses...
        </>
      }
      backup={<p>Étape {getStep} sur 3</p>}
    >
      {
        getStep === 1 &&
        <React.Fragment>
          <Textfield label="Votre nom" change={handleChangeInput('lastname')} placeholder="Nom..." />
          <Textfield label="Votre prénom" change={handleChangeInput('firstname')} placeholder="Prénom..." />
          <Textfield type="select" options={selectOptionsProfile} label="Votre profil" change={handleChangeInput('profile')} placeholder="Profil..." />
        </React.Fragment>
      }
      {
        getStep === 2 &&
        <React.Fragment>
          <div className="back" onClick={handlePreviousStep}>Étape précédente</div>
          <Textfield label="Votre numéro de téléphone" change={handleChangeInput('phone')} placeholder="Téléphone..." />
          <Textfield label="Votre adresse mail" change={handleChangeInput('email')} placeholder="Email..." />
          <Textfield label="Votre mot de passe" change={handleChangeInput('password')} type="password" placeholder="Mot de passe..." />
        </React.Fragment>
      }
      {
        getStep === 3 &&
        <React.Fragment>
          <div className="back" onClick={handlePreviousStep}>Étape précédente</div>
          <Textfield label="Votre société" change={handleChangeInput('company')} placeholder="Société..." />
          <Textfield label="Votre numéro de SIRET" change={handleChangeInput('siret')} placeholder="SIRET..." />
          <Textfield type="select" options={selectOptionsStatus} label="Statut de société" change={handleChangeInput('status')} placeholder="Statut..." />
        </React.Fragment>
      }
      <div className="center">
        {
          getStep !== 3
          ? <Button color="emeraud" click={handleNextStep}>Continuer</Button>
          : <Button color="emeraud" click={handleSubmit}>S'inscrire</Button>
        }
      </div>
      <div className="center"><Link to="/login" className="link-space">J'ai déjà compte</Link></div>
    </AuthLayout>
  )
}

export default Register