import "./error.scss"
import { Link } from "react-router-dom"
import Card from "../../components/card/Card"
import Logo from "../../components/logo/Logo"
import React from "react"

const Error = () => (
  <section className="container">
    <Logo color="white" size="10em" />
    <Card>La page que vous demandez n'existe pas. Retour à <Link to="/">l'accueil</Link>.</Card>
  </section>
)

export default Error