import "./home.scss"
import { Link } from "react-router-dom"
import { useCookies } from "react-cookie"
import Button from "../../components/button/Button"
import Card from "../../components/card/Card"
import facebookImage from "../../assets/images/facebook.svg"
import facebookWhiteImage from "../../assets/images/facebook-white.svg"
import linkedinImage from "../../assets/images/linkedin.svg"
import linkedinWhiteImage from "../../assets/images/linkedin-white.svg"
import Logo from "../../components/logo/Logo"
import React from "react"
import realTimeImage from "../../assets/images/home/icon-3.svg"
import reportingImage from "../../assets/images/home/icon-4.svg"
import Social from "../../components/social/Social"
import timeSavingImage from "../../assets/images/home/icon-2.svg"
import twitterImage from "../../assets/images/twitter.svg"
import twitterWhiteImage from "../../assets/images/twitter-white.svg"
import utilitiesImage from "../../assets/images/home/icon-1.svg"

const Home = () => {
  const [cookies] = useCookies(['token'])

  return (
    <main>
      <header>
        <nav>
          <div><Link to="/" title="We Dev" className="reset-opacity"><Logo color="white" size="10em" /></Link></div>
          <ul><li><Button link={cookies.token ? '/company' : '/login'} color="transparentDarkBlue">Se connecter</Button></li></ul>
        </nav>
        <div className="informations">
          <h1>L'outil idéal et adapté pour les <span>Freelances</span></h1>
          <Button link="/register" color="blue" rounded>Inscrivez-vous</Button>
        </div>
        <div className="socials">
          <Social link="https://www.facebook.com" title="Page Facebook" icon={facebookImage} />
          <Social link="https://twitter.com" title="Page Twitter" icon={twitterImage} />
          <Social link="https://www.linkedin.com" title="Page Linkedin" icon={linkedinImage} />
        </div>
      </header>
      <section id="blue">
        <div className="center"><Button link="/register" color="transparentSnow">Découvrir WeDev</Button></div>
        <h2>Application de gestion de projet pour les devs</h2>
        <p>
          Notre solution vous simplifiera grandement votre gestion de projet, vous gagnerez du temps que vous pourrez consacrer à d'autres choses plus importantes.<br/>
          Gérez vos clients, vos tâches, obtenez des rapports complets, connectez vos projets avec Git, et bien d'autres choses...
        </p>
        <div className="cases">
          <div className="case">
            <img src={reportingImage} alt="Reporting" />
            <div>Vos reporting centralisés</div>
          </div>
          <div className="case">
            <img src={realTimeImage} alt="Temps réel" />
            <div>Le suivi de vos activités en temps réel</div>
          </div>
          <div className="case">
            <img src={timeSavingImage} alt="Gain de temps" />
            <div>Un gain de temps pour le pilotage</div>
          </div>
          <div className="case">
            <img src={utilitiesImage} alt="Utilitaires" />
            <div>Des utilitaires pour votre dev</div>
          </div>
        </div>
      </section>
      <section id="white">
        <div className="col">
          <h3>Tous vos outils de dev réuni sur une seule application</h3>
          <p>
            Obtenez des métriques complètes sur votre activité (projets réalisés, en cours, chiffre d'affaire, moyenne du coup horaire, etc...).
            Vous obtiendrez toutes vos statistiques facilement et rapidement.
          </p>
          <p>
            En connectant vos projets avec GitHub vous pourrez plus facilement gérer vos clients, vos sprints et vos tâches.
            Vous obtiendez directement dans l'interface les informations de vos projets.
          </p>
        </div>
        <div className="col">
          <div className="point">
            <div><span>1</span></div>
            <div>
              <div className="title">Créér vos projets</div>
              <p>Associez un projet à un client et laissez vous guidez. L'outil vous accompagnera pour que vous puissiez optimiser votre gestion de projet.</p>
            </div>
          </div>

          <div className="point">
            <div><span>2</span></div>
            <div>
              <div className="title">Ajouter vos sprints</div>
              <p>Découpez automatiquement ou non le temps à consacrer à chaque projet pour mieux gérer son planning.</p>
            </div>
          </div>

          <div className="point">
            <div><span>3</span></div>
            <div>
              <div className="title">Affectez vos tâches</div>
              <p>Suivez vos tâches par catégories, par priorité de manière simple et efficace. Vous n'oublirez plus rien.</p>
            </div>
          </div>
        </div>
      </section>
      <footer>
        <div className="illustration">
          <Card>
            <div className="title">Offre de lancement</div>
            <p>Inscrivez-vous et recevez prochainement un accès premium à l'outil We Are Dev</p>
            <Button link="/register" color="blue">Je m'inscris</Button>
          </Card>
        </div>
        <div className="line">
          <div><Logo color="dark" size="10em" /></div>
          <div>&copy Copyright {new Date().getFullYear()} - We Are Dev</div>
          <div>
            <Social link="https://www.facebook.com" title="Page Facebook" icon={facebookWhiteImage} size="small" />
            <Social link="https://twitter.com" title="Page Twitter" icon={twitterWhiteImage} size="small" />
            <Social link="https://www.linkedin.com" title="Page Linkedin" icon={linkedinWhiteImage} size="small" />
          </div>
        </div>
      </footer>
    </main>
  )
}

export default Home