import initialState from "./state"
import actions from "./action"

export default (state = initialState, action) => {
  const { type, value } = action
  switch (type) {
    case actions.SET_ID: {
      return { ...state, id: value }
    }

    case actions.SET_FIRSTNAME: {
      return { ...state, firstname: value }
    }

    case actions.SET_LASTNAME: {
      return { ...state, lastname: value }
    }

    case actions.SET_SUCCESS: {
      return { ...state, success: value }
    }

    case actions.SET_ERROR: {
      return { ...state, error: value }
    }

    default:
      return state
  }
}
