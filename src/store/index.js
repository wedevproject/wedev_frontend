import { createStore, applyMiddleware } from "redux"
import { composeWithDevTools } from "redux-devtools-extension"
import thunk from "redux-thunk"
import reducer from "./reducer"
import Utils from "../services/Utils"

let middlewares = applyMiddleware(thunk)

if (Utils.isDevEnv()) {
  middlewares = composeWithDevTools(middlewares)
}

export function makeStore() {
  return createStore(reducer, middlewares)
}