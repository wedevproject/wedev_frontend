export default {
  SET_ID: "set id",
  SET_FIRSTNAME: "set firstname",
  SET_LASTNAME: "set lastname",
  SET_SUCCESS: "set success",
  SET_ERROR: "set error",
}
