import "react-app-polyfill/ie11"
import "react-app-polyfill/stable"
import { BrowserRouter as Router } from "react-router-dom"
import { CookiesProvider } from "react-cookie"
import { makeStore } from "./store"
import { StoreContext } from "redux-react-hook"
import App from "./components/app/App"
import React from "react"
import ReactDOM from "react-dom"

const store = makeStore()

ReactDOM.render(
  <StoreContext.Provider value={store}>
    <Router>
      <CookiesProvider>
        <App />
      </CookiesProvider>
    </Router>
  </StoreContext.Provider>,
  document.getElementById("root")
)
