import "./button.scss"
import { Link } from "react-router-dom"
import clsx from "clsx"
import PropTypes from "prop-types"
import React from "react"

const Button = ({ children, color, rounded, click, link, title, className }) => {
  const buttonClass = `button--${color}`

  if (link) {
    return <Link to={link} title={title} className={clsx(`button ${buttonClass} ${className}`, {"button-rounded": rounded})}>{children}</Link>
  }

  return <button onClick={click} className={clsx(`button ${buttonClass} ${className}`, {"button-rounded": rounded})}>{children}</button>
}

Button.propTypes = {
  children: PropTypes.node,
  color: PropTypes.string,
  rounded: PropTypes.bool,
  click: PropTypes.func,
  link: PropTypes.string,
  title: PropTypes.string,
  className: PropTypes.string
}

export default Button
