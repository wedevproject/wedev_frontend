import "./textfield.scss"
import clsx from "clsx"
import PropTypes from "prop-types"
import React, { useState } from "react"
import Select from "react-select"
import styles from "./textfieldStyles"
import Utils from "../../services/Utils"

const Textfield = ({ label, type, value, change, placeholder, maxLength, options }) => {
  const [getInvalid, setInvalid] = useState(false)
  const id = Math.random().toString(36).slice(2)

  const handleChange = e => {
    let value = null

    if (e.target) {
      value = e.target.value
    }
    else {
      value = e.value
    }

    if (type === "email" && !Utils.validateEmail(value)) {
      setInvalid(true)
    }
    else {
      setInvalid(false)

      if (change) {
        change(value)
      }
    }
  }

  return (
    <div className="line">
      {label && <label htmlFor={id}>{label}</label>}
      {
        type !== "select"
        ?
          <input
            id={id}
            type={type || "text"}
            value={value}
            onChange={handleChange}
            placeholder={placeholder}
            maxLength={maxLength}
            className={clsx("textfield", {"textfield-invalid": getInvalid})}
          />
        :
          <Select
            id={id}
            options={options}
            value={value}
            onChange={handleChange}
            styles={styles}
            placeholder={placeholder}
            noOptionsMessage={() => "Élement introuvable"}
          />
      }
    </div>
  )
}

Textfield.propTypes = {
  label: PropTypes.string,
  type: PropTypes.oneOf(["text", "email", "number", "date", "password", "tel", "select"]),
  options: PropTypes.array,
  change: PropTypes.func,
  placeholder: PropTypes.string,
  maxLength: PropTypes.string,
}

export default Textfield