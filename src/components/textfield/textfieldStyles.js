const light = '#ecf0f1'
const dark = '#95a5a6'
const black = '#434242'
const snow = '#ffffff'

const styles = {
  container: (provided) => ({
    ...provided,
    width: '100%',
  }),
  control: (provided) => ({
    ...provided,
    borderRadius: 4,
    boxShadow: `0 0 0 1px ${light}`,
    border: 'none',
    paddingLeft: 7,
    paddingRight: 7,
    paddingTop: 5,
    paddingBottom: 5,
  }),
  input: (provided) => ({
    ...provided,
    margin: 0,
    fontFamily: "Montserrat, Arial",
    color: dark,
    fontSize: 15,
  }),
  option: (provided) => ({
    ...provided,
    color: black,
    backgroundColor: snow,
    "&:hover": {
      color: black,
      backgroundColor: light,
    }
  }),
  dropdownIndicator: (provided) => ({
    ...provided,
    color: dark,
  }),
  indicatorSeparator: (provided) => ({
    ...provided,
    display: "none !important",
  }),
  placeholder: (provided) => ({
    ...provided,
    color: black,
    opacity: 0.6,
  }),
}

export default styles