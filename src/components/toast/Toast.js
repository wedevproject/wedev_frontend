import "./toast.scss"
import closeImageDark from "../../assets/images/close.svg"
import closeImageWhite from "../../assets/images/close-white.svg"
import clsx from "clsx"
import PropTypes from "prop-types"
import React, { useState } from "react"

const Toast = ({ children, color }) => {
  const [getClose, setClose] = useState(false)
  const toastClass = `toast--${color}`

  const handleClose = () => {
    setClose(true)
  }

  setTimeout(() => {
    setClose(true)
  }, 6000)
  
  return (
    <div className={clsx(`toast ${toastClass}`, {"hidden-toast": getClose})}>
      <img src={color === 'lightGrey' ? closeImageDark : closeImageWhite} className="close" onClick={handleClose} alt="Fermer" />
      {children}
    </div>
  )
}

Toast.propTypes = {
  children: PropTypes.node,
  color: PropTypes.string,
}

export default Toast