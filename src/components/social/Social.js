import "./social.scss"
import PropTypes from "prop-types"
import React from "react"
import clsx from "clsx"

const Social = ({ link, title, icon, size }) => (
  <a href={link} title={title} target="_blank" rel="noopener noreferrer" className={clsx("social", {"social-small": size === "small"})}>
    <img src={icon} alt={title} />
  </a>
)

Social.propTypes = {
  link: PropTypes.string,
  title: PropTypes.string,
  icon: PropTypes.node,
  size: PropTypes.string,
}

export default Social