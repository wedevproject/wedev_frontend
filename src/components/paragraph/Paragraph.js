import PropTypes from "prop-types"
import React from "react"

const Paragraph = ({ children }) => (
  <p>{children}</p>
)

Paragraph.propTypes = {
  children: PropTypes.node,
}

export default Paragraph