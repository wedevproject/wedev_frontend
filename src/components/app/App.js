import "../../assets/scss/styles.scss"
import { Switch, Route } from "react-router-dom"
import { withCookies } from "react-cookie"
import { useMappedState, useDispatch } from "redux-react-hook"
import actions from "../../store/action"
import Client from "../../pages/dashboard/client/Client"
import Clients from "../../pages/dashboard/clients/Clients"
import Company from "../../pages/dashboard/company/Company"
import Error from "../../pages/error/Error"
import Home from "../../pages/home/Home"
import Login from "../../pages/login/Login"
import Password from "../../pages/password/Password"
import Project from "../../pages/dashboard/project/Project"
import React from "react"
import Register from "../../pages/register/Register"
import Sprint from "../../pages/dashboard/sprint/Sprint"
import Task from "../../pages/dashboard/task/Task"
import Tasks from "../../pages/dashboard/tasks/Tasks"
import Toast from "../toast/Toast"
import User from "../../pages/dashboard/user/User"
import Utils from "../../services/Utils"

const App = ({ cookies }) => {
  const dispatch = useDispatch()
  const mapState = state => ({ success: state.success, error: state.error })
  const { success, error } = useMappedState(mapState)
  const token = cookies.get('token')
  let authenticate = false

  if (token) {
    const chars = token.split('.')

    if (chars[1]) {
      let payload = atob(chars[1])
  
      if (Utils.isJSONString(payload)) {
        payload = JSON.parse(payload)
        const id = payload.user_id
        const firstname = payload.user_first_name
        const lastname = payload.user_last_name
        
        if (id && firstname && lastname) {
          dispatch({ type: actions.SET_ID, value: id })
          dispatch({ type: actions.SET_FIRSTNAME, value: firstname })
          dispatch({ type: actions.SET_LASTNAME, value: lastname })
          authenticate = true
        }
      }
    }
  }

  const AnonymousRoute = ({ ...options }) => {
    return !authenticate ? <Route {...options} /> : <Route render={props => <Company {...props} />} />
  }

  const PrivateRoute = ({ ...options }) => {
    return authenticate ? <Route {...options} /> : <Route render={props => <Login {...props} />} />
  }

  return (
    <React.Fragment>
      {success && <Toast color="emeraud">{success}</Toast>}
      {error && <Toast color="red">{error}</Toast>}
      <Switch>
        <Route exact path="/" render={props => <Home {...props} />} />
        <AnonymousRoute exact path="/login" render={props => <Login {...props} />} />
        <AnonymousRoute exact path="/register" render={props => <Register {...props} />} />
        <AnonymousRoute exact path="/password" render={props => <Password {...props} />} />
        <PrivateRoute exact path="/company" render={props => <Company {...props} />} />
        <PrivateRoute exact path="/account" render={props => <User {...props} />} />
        <PrivateRoute exact path="/client" render={props => <Client {...props} />} />
        <PrivateRoute exact path="/clients" render={props => <Clients {...props} />} />
        <PrivateRoute exact path="/project" render={props => <Project {...props} />} />
        <PrivateRoute exact path="/sprint" render={props => <Sprint {...props} />} />
        <PrivateRoute exact path="/task" render={props => <Task {...props} />} />
        <PrivateRoute exact path="/tasks" render={props => <Tasks {...props} />} />
        <Route component={Error} />
      </Switch>
    </React.Fragment>
  )
}

export default withCookies(App)