import logoWhiteImage from  "../../assets/images/logo-white.svg"
import logoDarkImage from "../../assets/images/logo-dark.svg"
import PropTypes from "prop-types"
import React from "react"

const Logo = ({ color, size }) => (
  <img src={color === "dark" ? logoDarkImage : logoWhiteImage} style={{width: size || '13em'}} className="logo" alt="WeDev" />
)

Logo.propTypes = {
  color: PropTypes.string,
  size: PropTypes.string,
}

export default Logo