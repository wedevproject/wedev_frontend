import "./appLayout.scss"
import { createBrowserHistory } from "history"
import { Link } from "react-router-dom"
import { useMappedState } from "redux-react-hook"
import Button from "../button/Button"
import clsx from "clsx"
import Logo from "../../components/logo/Logo"
import PropTypes from "prop-types"
import React from "react"
import Utils from "../../services/Utils"

const AppLayout = ({ title, children, actionLabel, actionUrl }) => {
  const mapState = state => ({ firstname: state.firstname })
  const { firstname } = useMappedState(mapState)

  const verifyPath = (pathToCheck) => {
    const history = createBrowserHistory()
    const pathname = Utils.replaceSlash(history.location.pathname)

    if (pathname === pathToCheck) {
      return true
    }

    return false
  }

  return (
    <div className="appLayout">
      <header>
        <div><Link to="/"><Logo color="white" size="10em" /></Link></div>
        <nav>
          <ul>
            <li><Link to="/contact" title="Support">Support</Link></li>
            <li><Link to="/account" title="Mon compte">Mon compte ({firstname})</Link></li>
          </ul>
        </nav>
      </header>
      <section>
        <div className="col">
          <div className="col-space">
            <div className="title">Toutes les options</div>
            <div className="links">
              <Link to="/company" title="Ma société" className={clsx({"active": verifyPath('company')})}>Ma société</Link>
              <Link to="/clients" title="Tous mes clients" className={clsx({"active": verifyPath('clients') || verifyPath('client')})}>Tous mes clients</Link>
              <Link to="/projects" title="Tous mes projets" className={clsx({"active": verifyPath('projects') || verifyPath('project')})}>Tous mes projets</Link>
              <Link to="/tasks" title="Tâches à réaliser" className={clsx({
                "active": verifyPath('tasks') || verifyPath('task') || verifyPath('sprints') || verifyPath('sprint')
              })}>
                Tâches à réaliser
              </Link>
            </div>
            <div className="link-space">
              <Link to="/more" title="Tâches à réaliser">Plus d'options</Link>
            </div>
          </div>
        </div>
        <div className="col">
          <div className="col-space">
            <div className="title">{title}</div>
            {actionLabel && actionUrl && <Button link={actionUrl} color="blue" className="action-button">{actionLabel}</Button>}
            {children}
          </div>
        </div>
      </section>
    </div>
  )
}

AppLayout.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
  actionLabel: PropTypes.string,
  actionUrl: PropTypes.string,
}

export default AppLayout