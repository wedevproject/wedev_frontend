import "./authLayout.scss"
import Logo from "../../components/logo/Logo"
import PropTypes from "prop-types"
import React from "react"

const AuthLayout = ({ title, image, text, children, backup }) => (
  <div className="authLayout">
    <div className="col align-vertical">
      <div className="center space-top">
        <Logo color="white" size="10em" />
        <span>{title}</span>
      </div>
      <form className="center-block">
        {children}
      </form>
      <div className="center space-bottom">
        {backup}
      </div>
    </div>
    <div className="col center-vertical">
      <div className="center">
        <img src={image} alt="We Dev" />
        <p>{text}</p>
      </div>
    </div>
  </div>
)

AuthLayout.propTypes = {
  title: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired,
  text: PropTypes.node.isRequired,
  children: PropTypes.node.isRequired,
  backup: PropTypes.node,
}

export default AuthLayout