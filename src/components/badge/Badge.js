import "./badge.scss"
import PropTypes from "prop-types"
import React from "react"
import clsx from "clsx"

const Badge = ({ children, color }) => {
  const badgeClass = `badge--${color}`
  
  return (
    <div className={clsx(`badge ${badgeClass}`)}>{children}</div>
  )
}

Badge.propTypes = {
  children: PropTypes.node.isRequired,
  color: PropTypes.string.isRequired,
}

export default Badge