import "./card.scss"
import closeImage from "../../assets/images/close.svg"
import clsx from "clsx"
import PropTypes from "prop-types"
import React, { useState } from "react"

const Card = ({ children, close }) => {
  const [getClose, setClose] = useState(false)

  const handleClose = () => {
    if (close) {
      setClose(true)
    }
  }

  return (
    <div className={clsx("card", {"hidden-card": getClose})}>
      {close && <img src={closeImage} className="close" onClick={handleClose} alt="Fermer" />}
      {children}
    </div>
  )
}

Card.propTypes = {
  children: PropTypes.node,
  close: PropTypes.bool,
}

export default Card